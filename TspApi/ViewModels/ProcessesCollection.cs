﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace TspApi.ViewModels
{
    public class ProcessesCollection
    {
        [JsonPropertyName("process-list")]
        public List<string> ProcessInfos { get; set; }

        [JsonPropertyName("count")]
        public int Count { get { return ProcessInfos.Count; } }
    }
}
