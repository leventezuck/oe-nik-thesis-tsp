﻿using System.Text.Json.Serialization;

namespace TspApi.ViewModels
{
    public class ProcessIdResponse
    {
        [JsonPropertyName("process-id")]
        public string ProcessId { get; set; }
    }
}
