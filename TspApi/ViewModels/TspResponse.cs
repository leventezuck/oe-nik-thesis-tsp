﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;
using TspApi.Models;
using TspApi.Models.DatabaseEntities;
using TspApi.Models.Enums;

namespace TspApi.ViewModels
{
    public class TspResponse
    {
        [JsonPropertyName("process-id")]
        public string ProcessId { get; set; }

        [JsonPropertyName("algorirthm-type")]
        public AlgorithmType AlorithmType { get; set; }

        [JsonPropertyName("status")]
        public Status Status { get; set; }

        [JsonPropertyName("iteration")]
        public int Iteration { get; set; }

        [JsonPropertyName("fitness")]
        public float Fitness { get; set; }

        [JsonPropertyName("elapsed-time")]
        public TimeSpan ElapsedTime { get; set; }

        [JsonPropertyName("route")]
        public List<City> Route { get; set; }

        public static TspResponse ConvertToTspResponse(Snapshot snapshot)
        {
            return new TspResponse
            {
                ProcessId = snapshot.ProcessId,
                AlorithmType = snapshot.AlorithmType,
                Status = snapshot.Status,
                Iteration = snapshot.Iteration,
                Fitness = snapshot.Fitness,
                ElapsedTime = snapshot.ElapsedTime,
                Route = snapshot.Route
            };
        }
    }
}
