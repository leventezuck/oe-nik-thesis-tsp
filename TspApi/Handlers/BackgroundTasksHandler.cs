﻿using System;
using System.Collections.Generic;
using System.Threading;
using TspApi.Logic;
using TspApi.Models;
using TspApi.Models.Enums;

namespace TspApi.Handlers
{
    public sealed class BackgroundTasksHandler
    {
        private static BackgroundTasksHandler instance;

        public static BackgroundTasksHandler Instance { get { return instance ??= new BackgroundTasksHandler(); } }

        private Dictionary<Guid, CancellationTokenSource> processes;

        private BackgroundTasksHandler()
        {
            processes = new Dictionary<Guid, CancellationTokenSource>();

            ThreadPool.SetMinThreads(1, 0);
            ThreadPool.SetMaxThreads(Environment.ProcessorCount, 0);
        }

        public Guid Start(AlgorithmType type, string dataset)
        {
            CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();

            ISolution solution;

            switch (type)
            {
                case AlgorithmType.Genetic:
                    solution = new GeneticAlgorithm();
                    break;
                case AlgorithmType.NearestNeighbourBasic:
                    solution = new NearestNeighbourBasic();
                    break;
                case AlgorithmType.NearestNeighbourFull:
                    solution = new NearestNeighbourFull();
                    break;
                case AlgorithmType.ParticalSwarmOptimization:
                    solution = new ParticalSwarmOptimization();
                    break;
                case AlgorithmType.SimulatedAnnealing:
                    solution = new SimulatedAnnealing();
                    break;
                default:
                    throw new Exception();
            }

            List<City> cities = RouteGenerator.GetRoute(dataset);

            ThreadPool.QueueUserWorkItem(x => solution.Solve(cancellationTokenSource.Token, cities));

            processes.Add(solution.ProcessID, cancellationTokenSource);

            return solution.ProcessID;
        }

        public void Stop(Guid guid)
        {
            if (processes.TryGetValue(guid, out CancellationTokenSource cancellationTokenSource))
            {
                cancellationTokenSource.Cancel();
            }
        }
    }
}
