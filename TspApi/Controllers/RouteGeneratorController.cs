﻿using Microsoft.AspNetCore.Mvc;
using TspApi.Logic;

namespace TspApi.Controllers
{
    [Route("api/tsp/generate")]
    [ApiController]
    public class RouteGeneratorController : ControllerBase
    {
        [HttpGet("random")]
        public ActionResult GenerateRandomRoute(int cities = 50)
        {
            return Ok(RouteGenerator.GenerateRoute(cities));
        }
    }
}
