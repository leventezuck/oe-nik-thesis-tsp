﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using TspApi.Handlers;
using TspApi.Models.DatabaseEntities;
using TspApi.Models.Enums;
using TspApi.Services;
using TspApi.ViewModels;

namespace TspApi.Controllers
{
    [Route("api/tsp")]
    [ApiController]
    public class HandlerController : ControllerBase
    {

        [HttpGet("start")]
        public ActionResult Start(int type, string dataset = "berlin52")
        {
            return Ok(new ProcessIdResponse
            {
                ProcessId = StartProcess((AlgorithmType)type, dataset)
            });
        }

        private string StartProcess(AlgorithmType type, string dataset)
        {
            Guid processId = BackgroundTasksHandler.Instance.Start(type, dataset);

            ProcessRepository.Instance.Create(new Models.DatabaseEntities.Process
            {
                Id = processId.ToString(),
                Status = Status.running,
                Type = type
            });

            return processId.ToString();
        }

        [HttpGet("get")]
        public ActionResult Get(string id)
        {
            Snapshot snapshot = SnapshotRepository.Instance.Get(id).LastOrDefault();

            if (snapshot == null)
            {
                return NotFound();
            }

            return Ok(TspResponse.ConvertToTspResponse(snapshot));
        }

        [HttpGet("get-snapshots")]
        public ActionResult GetSnapshots(string id)
        {
            return Ok(SnapshotRepository.Instance.Get(id).Select(x => TspResponse.ConvertToTspResponse(x)).ToList());
        }

        [HttpGet("stop")]
        public ActionResult Stop(string id)
        {
            BackgroundTasksHandler.Instance.Stop(Guid.Parse(id));

            return Ok();
        }
    }
}
