﻿using Microsoft.AspNetCore.Mvc;
using System.Linq;
using TspApi.Models.Enums;
using TspApi.Services;
using TspApi.ViewModels;

namespace TspApi.Controllers
{
    [Route("api/tsp/statistics")]
    [ApiController]
    public class StatisticsController : ControllerBase
    {
        [HttpGet("process-list")]
        public ActionResult GetProcessesList()
        {
            return Ok(new ProcessesCollection
            {
                ProcessInfos = ProcessRepository.Instance.Get().Select(x => x.Id).ToList()
            });
        }

        [HttpGet("process-list/running")]
        public ActionResult GetRunningProcessesList()
        {
            return Ok(new ProcessesCollection
            {
                ProcessInfos = ProcessRepository.Instance.Get().Where(x => x.Status == Status.running).Select(x => x.Id).ToList()
            });
        }

        [HttpGet("process-list/stopped")]
        public ActionResult GetStoppedProcessesList()
        {
            return Ok(new ProcessesCollection
            {
                ProcessInfos = ProcessRepository.Instance.Get().Where(x => x.Status == Status.stopped).Select(x => x.Id).ToList()
            });
        }
    }
}
