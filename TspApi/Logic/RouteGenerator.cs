﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using TspApi.Models;

namespace TspApi.Logic
{
    public class RouteGenerator
    {
        private static Random rnd = new Random();

        public static List<City> GenerateRoute(int numberOfCities = 100)
        {
            List<City> cities = new List<City>();

            while (cities.Count < numberOfCities)
            {
                City city = new City(rnd.Next(numberOfCities * 10), rnd.Next(numberOfCities * 10));

                if (cities.Where(x => x.X == city.X || x.Y == city.Y).FirstOrDefault() == null)
                {
                    cities.Add(city);
                }
            }

            return cities;
        }

        public static List<City> LoadFromTSPFile(string filename = "berlin52.tsp")
        {
            List<City> cities = new List<City>();
            string[] lines = File.ReadAllLines("Datasets\\" + filename);

            for (int i = 6; i < lines.Length - 1; i++)
            {
                string[] columns = lines[i].Split(' ');
                cities.Add(new City(float.Parse(columns[1], CultureInfo.InvariantCulture.NumberFormat), float.Parse(columns[2], CultureInfo.InvariantCulture.NumberFormat)));
            }

            return cities;
        }

        public static List<City> GetRoute(string dataset)
        {
            List<City> cities;

            if (dataset.Contains("r-"))
            {
                cities = GenerateRoute(int.Parse(dataset.Split('-')[1]));
            }
            else
            {
                cities = LoadFromTSPFile(dataset + ".tsp");
            }

            return cities;
        }
    }
}
