﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using TspApi.Models;
using TspApi.Models.DatabaseEntities;
using TspApi.Models.Enums;
using TspApi.Models.SimulatedAnnealing;
using TspApi.Services;
using TspApi.ViewModels;

namespace TspApi.Logic
{
    public class SimulatedAnnealing : ISolution
    {
        private static Stopwatch stopwatch = new Stopwatch();
        private static Random rnd = new Random();

        private const int NEIGHBORS = 100;
        private const int MAXIMUM_TEMPERATURE = 1000;
        private const int MINIMUM_TEMPERATURE = 0;
        private const float TEMPERATURE_DECREASE_RATE = 1f;

        private Route gBest;
        private int iteration;

        public Guid ProcessID { get; set; }

        public SimulatedAnnealing()
        {
            ProcessID = Guid.NewGuid();
        }

        public void Solve(CancellationToken token, List<City> cities = null)
        {
            if (cities == null)
            {
                cities = RouteGenerator.LoadFromTSPFile();
            }

            gBest = new Route(cities);
            iteration = 0;

            float currentTemperature = MAXIMUM_TEMPERATURE;

            stopwatch.Start();

            UpdateState(Status.running);

            while (!token.IsCancellationRequested && currentTemperature > MINIMUM_TEMPERATURE)
            {
                Route pBest = new Route(gBest);

                for (int i = 0; i < NEIGHBORS; i++)
                {
                    float currentRouteEnergy = GetCurrentRouteEnergy(pBest);

                    Route neighborRoute = GenerateNeighbor(pBest);

                    float neighborRouteEnergy = GetCurrentRouteEnergy(neighborRoute);

                    float deltaEnergy = neighborRouteEnergy - currentRouteEnergy;

                    if (deltaEnergy < 0 || AcceptBadRoute(deltaEnergy, currentTemperature))
                    {
                        pBest = new Route(neighborRoute);
                    }
                }

                if (pBest.Distance < gBest.Distance)
                {
                    gBest = new Route(pBest);

                    UpdateState(Status.running);
                }

                currentTemperature -= TEMPERATURE_DECREASE_RATE;
                iteration++;
            }

            stopwatch.Stop();

            UpdateState(Status.stopped);
        }

        private float GetCurrentRouteEnergy(Route route)
        {
            return route.Distance;
        }

        private Route GenerateNeighbor(Route route)
        {
            Route neighbor = new Route(route);

            int r1 = rnd.Next(0, neighbor.Cities.Count);
            int r2 = rnd.Next(0, neighbor.Cities.Count);

            while (r1 == r2)
            {
                r1 = rnd.Next(0, neighbor.Cities.Count);
                r2 = rnd.Next(0, neighbor.Cities.Count);
            }

            City c1 = neighbor.Cities[r1];
            City c2 = neighbor.Cities[r2];

            neighbor.Cities[r1] = c2;
            neighbor.Cities[r2] = c1;

            return neighbor;
        }

        private bool AcceptBadRoute(float deltaEnergy, float currentTemperature)
        {
            if (deltaEnergy == 0 || currentTemperature == 0)
            {
                return false;
            }

            return Math.Exp(-deltaEnergy / currentTemperature) >= rnd.NextDouble();
        }

        private void UpdateState(Status status)
        {
            TspResponse tspResponse = new TspResponse
            {
                ProcessId = ProcessID.ToString(),
                Status = status,
                AlorithmType = AlgorithmType.Genetic,
                Iteration = iteration,
                Route = gBest.Cities,
                Fitness = gBest.Distance,
                ElapsedTime = stopwatch.Elapsed
            };

            SnapshotRepository.Instance.Create(Snapshot.ConvertToSnapshot(tspResponse));

            ProcessRepository.Instance.Update(new Models.DatabaseEntities.Process { Id = ProcessID.ToString(), Status = status, Type = AlgorithmType.Genetic });
        }
    }
}
