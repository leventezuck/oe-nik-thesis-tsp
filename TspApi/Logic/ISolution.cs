﻿using System;
using System.Collections.Generic;
using System.Threading;
using TspApi.Models;
using TspApi.ViewModels;

namespace TspApi.Logic
{
    public interface ISolution
    {
        Guid ProcessID { get; set; }

        void Solve(CancellationToken token, List<City> cities = null);
    }
}
