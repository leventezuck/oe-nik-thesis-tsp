﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using TspApi.Models;
using TspApi.Models.DatabaseEntities;
using TspApi.Models.Enums;
using TspApi.Models.GeneticAlgorithm;
using TspApi.Services;
using TspApi.ViewModels;

namespace TspApi.Logic
{
    public class GeneticAlgorithm : ISolution
    {
        private static Random rnd = new Random();
        private static Stopwatch stopwatch = new Stopwatch();

        private int iteration;
        private Chromosome pBest;

        public Guid ProcessID { get; set; }
        public bool Stopped { get; set; }
        public int NumberOfPopulation { get; set; }
        public double RateOfElitism { get; set; }
        public double RateOfMutation { get; set; }
        public List<Chromosome> Result { get; set; }

        public GeneticAlgorithm()
        {   
            NumberOfPopulation = 50;
            RateOfMutation = 0.01;
            RateOfElitism = 0.1;

            Result = new List<Chromosome>();

            ProcessID = Guid.NewGuid();
        }

        public void Solve(CancellationToken token, List<City> cities = null)
        {
            if (cities == null)
            {
                cities = RouteGenerator.LoadFromTSPFile();
            }

            stopwatch.Start();

            Population population = InitializePopulation(new Chromosome() { Cities = cities });
            pBest = Evaluation(population);
            Result.Add(pBest.Clone());
            iteration = 0;

            UpdateState(Status.running);

            while (!token.IsCancellationRequested)
            {
                if (pBest.Fitness < Result[Result.Count - 1].Fitness)
                {
                    Result.Add(pBest);
                    UpdateState(Status.running);
                }

                Population newPopulation = SelectParents(population);

                while (newPopulation.Chromosomes.Count <= NumberOfPopulation)
                {
                    Population matingPopulation = Selection(population);
                    Chromosome newChromosome = CrossOver(matingPopulation);
                    newChromosome = Mutate(newChromosome);
                    newPopulation.Chromosomes.Add(newChromosome);
                }

                population = newPopulation;
                pBest = Evaluation(population);

                iteration++;
            }

            stopwatch.Stop();

            UpdateState(Status.stopped);
        }

        private Chromosome Mutate(Chromosome chromosome)
        {
            for (int i = 0; i < chromosome.Cities.Count * RateOfMutation; i++)
            {
                int a = rnd.Next(0, chromosome.Cities.Count);
                int b = rnd.Next(0, chromosome.Cities.Count);

                City city = chromosome.Cities[a].Clone();
                chromosome.Cities[a] = chromosome.Cities[b].Clone();
                chromosome.Cities[b] = city.Clone();
            }

            return chromosome;
        }

        private Chromosome CrossOver(Population population)
        {
            Chromosome child = new Chromosome();

            Chromosome parent1 = population.Chromosomes[rnd.Next(0, population.Chromosomes.Count)];
            Chromosome parent2 = population.Chromosomes[rnd.Next(0, population.Chromosomes.Count)];

            int cutIndex = rnd.Next(0, parent1.Cities.Count);

            int i = 0;

            while (i < cutIndex)
            {
                child.Cities.Add(parent1.Cities[i]);
                i++;
            }

            foreach (City City in parent2.Cities)
            {
                if (child.Cities.Where(x => (x.X == City.X && x.Y == City.Y)).FirstOrDefault() == null)
                {
                    child.Cities.Add(City);
                }
            }

            return child;
        }

        private Population Selection(Population population)
        {
            Population matingPoppulation = new Population();

            for (int i = 0; i < rnd.Next(0, population.Chromosomes.Count); i++)
            {
                matingPoppulation.Chromosomes.Add(population.Chromosomes.OrderBy(x => x.Fitness).ToList()[i]);
            }

            while (matingPoppulation.Chromosomes.Count != population.Chromosomes.Count)
            {
                matingPoppulation.Chromosomes.Add(population.Chromosomes[rnd.Next(0, population.Chromosomes.Count)]);
            }

            return matingPoppulation;
        }

        private Population SelectParents(Population population)
        {
            population.Chromosomes = population.Chromosomes.OrderBy(x => x.Fitness).ToList();

            Population newPopulation = new Population();

            for (int i = 0; i < NumberOfPopulation * RateOfElitism / 4; i++)
            {
                newPopulation.Chromosomes.Add(population.Chromosomes[i]);
            }

            return newPopulation;
        }

        private Population InitializePopulation(Chromosome chromosome)
        {
            Population population = new Population();
            Chromosome newChromosome = chromosome;

            for (int i = 0; i < NumberOfPopulation; i++)
            {
                int n = newChromosome.Cities.Count;
                while (n > 1)
                {
                    n--;
                    int k = rnd.Next(n + 1);
                    City value = newChromosome.Cities[k];
                    newChromosome.Cities[k] = newChromosome.Cities[n];
                    newChromosome.Cities[n] = value;
                }

                population.Chromosomes.Add(newChromosome);
            }

            return population;
        }

        private Chromosome Evaluation(Population population)
        {
            Chromosome pBest = null;

            foreach (Chromosome chromosome in population.Chromosomes)
            {
                if (pBest == null || chromosome.Fitness < pBest.Fitness)
                {
                    pBest = chromosome.Clone();
                }
            }

            return pBest;
        }

        private void UpdateState(Status status)
        {
            TspResponse tspResponse = new TspResponse
            {
                ProcessId = ProcessID.ToString(),
                Status = status,
                AlorithmType = AlgorithmType.Genetic,
                Iteration = iteration,
                Route = pBest.Cities,
                Fitness = pBest.Fitness,
                ElapsedTime = stopwatch.Elapsed
            };

            SnapshotRepository.Instance.Create(Snapshot.ConvertToSnapshot(tspResponse));

            ProcessRepository.Instance.Update(new Models.DatabaseEntities.Process { Id = ProcessID.ToString(), Status = status, Type = AlgorithmType.Genetic });
        }
    }
}
