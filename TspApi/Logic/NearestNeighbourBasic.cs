﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using TspApi.Models;
using TspApi.Models.DatabaseEntities;
using TspApi.Models.Enums;
using TspApi.Services;
using TspApi.ViewModels;

namespace TspApi.Logic
{
    public class NearestNeighbourBasic : ISolution
    {
        public Guid ProcessID { get; set; }

        private static Stopwatch stopwatch = new Stopwatch();

        public List<City> Result { get; set; }
        public float Fitness { get; set; }
        public int Iteration { get; set; }
        public TimeSpan ElapsedTime { get; set; }

        public NearestNeighbourBasic()
        {
            Result = new List<City>();
            Fitness = float.MaxValue;
            ProcessID = Guid.NewGuid();
        }

        public void Solve(CancellationToken token, List<City> cities = null)
        {
            if (cities == null)
            {
                cities = RouteGenerator.LoadFromTSPFile();
            }

            stopwatch.Start();

            Fitness = GetFitness(cities);
            Result = cities;
            Iteration = 0;

            UpdateState(Status.running);

            List<City> actualResult = new List<City>();
            float actualFitness = 0f;

            actualResult.Add(cities[0]);

            for (int j = 0; j < cities.Count - 1; j++)
            {
                float nearestDistance = float.MaxValue;
                City nearestNeighbour = null;

                List<City> expectResult = cities.Except(actualResult).ToList();

                foreach (City city in expectResult)
                {
                    float calculatedDistance = DistanceBetweenOfTwoCities(actualResult[actualResult.Count - 1], city);
                    if (calculatedDistance < nearestDistance)
                    {
                        nearestDistance = calculatedDistance;
                        nearestNeighbour = city;
                    }
                }

                actualResult.Add(nearestNeighbour);
                actualFitness += nearestDistance;
            }

            Fitness = actualFitness;
            Result = actualResult;
            Iteration = 1;

            stopwatch.Stop();

            UpdateState(Status.stopped);
        }

        private float DistanceBetweenOfTwoCities(City from, City to)
        {
            return (float)Math.Sqrt(Math.Pow(from.X - to.X, 2) + Math.Pow(from.Y - to.Y, 2));
        }

        private float GetFitness(List<City> cities)
        {
            float sumOfDistance = 0f;

            for (int i = 0; i < cities.Count - 1; i++)
            {
                City c1 = cities[i];
                City c2 = cities[i + 1];

                sumOfDistance += (float)Math.Sqrt(Math.Pow(c1.X - c2.X, 2) + Math.Pow(c1.Y - c2.Y, 2));
            }

            return sumOfDistance;
        }

        private void UpdateState(Status status)
        {
            TspResponse tspResponse = new TspResponse
            {
                ProcessId = ProcessID.ToString(),
                Status = status,
                AlorithmType = AlgorithmType.NearestNeighbourBasic,
                Iteration = Iteration,
                Route = Result,
                Fitness = Fitness,
                ElapsedTime = stopwatch.Elapsed
            };

            SnapshotRepository.Instance.Create(Snapshot.ConvertToSnapshot(tspResponse));

            ProcessRepository.Instance.Update(new Models.DatabaseEntities.Process { Id = ProcessID.ToString(), Status = status, Type = AlgorithmType.NearestNeighbourBasic });
        }
    }
}
