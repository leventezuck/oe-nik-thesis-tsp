﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using TspApi.Models;
using TspApi.Models.DatabaseEntities;
using TspApi.Models.Enums;
using TspApi.Models.ParticalSwarmOptimization;
using TspApi.Services;
using TspApi.ViewModels;

namespace TspApi.Logic
{
    public class ParticalSwarmOptimization : ISolution
    {
        private static Stopwatch stopwatch = new Stopwatch();
        private static Random rnd = new Random();

        //public static readonly int MAXIMUM_PARTICLES = 20;
        //public static readonly int MAXIMUM_ITERATIONS = 500;
        //public static readonly int NEIGHBORS = 20;
        //public static readonly float C1 = 1.5f;
        //public static readonly float C2 = 1.5f;
        //public static readonly float LOW = 0;
        //public static readonly float HIGH = 1;

        public static readonly int MAXIMUM_PARTICLES = 80;
        public static readonly int MAXIMUM_ITERATIONS = 500;
        public static readonly int NEIGHBORS = 35;
        public static readonly float C1 = 2f;
        public static readonly float C2 = 2f;
        public static readonly float LOW = 0;
        public static readonly float HIGH = 1;

        private List<Particle> particles;
        private Location bestLocation;
        private Route bestRoute;

        int iteration;

        public Guid ProcessID { get; set; }

        public ParticalSwarmOptimization()
        {
            ProcessID = Guid.NewGuid();
            particles = new List<Particle>();
        }

        public void Solve(CancellationToken token, List<City> cities = null)
        {
            if (cities == null)
            {
                cities = RouteGenerator.LoadFromTSPFile();
            }

            stopwatch.Start();

            for (int i = 0; i < MAXIMUM_PARTICLES; i++)
            {
                particles.Add(new Particle(new Route(cities.OrderBy(x => Guid.NewGuid()).ToList())));
            }

            bestLocation = new Location(new List<float>());

            InitilizeVelocites();
            InitilizeLocations();

            Sort();

            bestRoute = new Route(particles[0].PBest.Cities);

            UpdateState(Status.running);

            float w = 0;
            iteration = 0;

            while (!token.IsCancellationRequested)
            {
                for (int i = 0; i < particles.Count - 1; i++)
                {
                    particles[i].GetBest();
                }

                Sort();

                if (particles[0].PBest.Distance < bestRoute.Distance)
                {
                    bestRoute = new Route(particles[0].PBest);
                    //bestLocation = new Location(particles[0].Location.Locations);

                    UpdateState(Status.running);
                }

                //w = HIGH - (iteration / MAXIMUM_ITERATIONS) * (HIGH - LOW);
                //w = HIGH - 1 * (HIGH - LOW);
                w = 0.72f;

                for (int i = 0; i < particles.Count - 1; i++)
                {
                    for (int j = 0; j < Particle.ProblemSize - 1; j++)
                    {
                        float random1 = (float)rnd.NextDouble();
                        float random2 = (float)rnd.NextDouble();

                        float velocity = particles[i].Velocity.Dimensions[j];
                        float location = particles[i].Location.Locations[j];
                        float pBestLocation = particles[i].LocationPBest.Locations[j];
                        float gBestLocation = bestLocation.Locations[j];

                        float newVelocity = (w * velocity) + (random1 * C1) * (pBestLocation - location) + (random2 * C2) * (gBestLocation - location);

                        particles[i].Velocity.Dimensions[j] = newVelocity;

                        float newPosition = location + newVelocity;

                        particles[i].Location.Locations[j] = newPosition;
                        particles[i].SwapWithLocation();
                    }
                }

                iteration++;
            }

            stopwatch.Stop();

            UpdateState(Status.stopped);
        }

        private void InitilizeVelocites()
        {
            foreach (Particle particle in particles)
            {
                for (int i = 0; i < Particle.ProblemSize - 1; i++) //Problem size
                {
                    particle.Velocity.Dimensions.Add((float)rnd.NextDouble() * 2 - 1);
                }
            }
        }

        private void InitilizeLocations()
        {
            foreach (Particle particle in particles)
            {
                for (int i = 0; i < particle.Route.Cities.Count - 1; i++)
                {
                    particle.Location.Locations.Add(i);
                    particle.LocationPBest.Locations.Add(i);
                }
            }

            for (int i = 0; i < Particle.ProblemSize - 1; i++)
            {
                bestLocation.Locations.Add(i);
            }
        }

        private void Sort()
        {
            particles = particles.OrderBy(x => x.PBest.Distance).ToList();
        }

        private void UpdateState(Status status)
        {
            TspResponse tspResponse = new TspResponse
            {
                ProcessId = ProcessID.ToString(),
                Status = status,
                AlorithmType = AlgorithmType.ParticalSwarmOptimization,
                Iteration = iteration,
                Route = bestRoute.Cities,
                Fitness = bestRoute.Distance,
                ElapsedTime = stopwatch.Elapsed
            };

            SnapshotRepository.Instance.Create(Snapshot.ConvertToSnapshot(tspResponse));

            ProcessRepository.Instance.Update(new Models.DatabaseEntities.Process { Id = ProcessID.ToString(), Status = status, Type = AlgorithmType.ParticalSwarmOptimization });
        }
    }
}
