﻿using MongoDB.Bson.Serialization.Attributes;
using TspApi.Models.Enums;

namespace TspApi.Models.DatabaseEntities
{
    public class Process
    {
        [BsonId]
        public string Id { get; set; }

        [BsonElement("type")]
        public AlgorithmType Type { get; set; }

        [BsonElement("status")]
        public Status Status { get; set; }
    }
}
