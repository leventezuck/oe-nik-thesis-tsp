﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using TspApi.Models.Enums;
using TspApi.ViewModels;

namespace TspApi.Models.DatabaseEntities
{
    public class Snapshot
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        [BsonElement("_id")]
        public string Id { get; set; }

        [BsonElement("process_id")]
        public string ProcessId { get; set; }

        [BsonElement("type")]
        public AlgorithmType AlorithmType { get; set; }

        [BsonElement("status")]
        public Status Status { get; set; }

        [BsonElement("iterations")]
        public int Iteration { get; set; }

        [BsonElement("fitness")]
        public float Fitness { get; set; }

        [BsonElement("created_at")]
        public DateTime CreatedAt { get; set; }

        [BsonElement("elapsed_time")]
        public TimeSpan ElapsedTime { get; set; }

        [BsonElement("route")]
        public List<City> Route { get; set; }

        public static Snapshot ConvertToSnapshot(TspResponse tspResponse)
        {
            return new Snapshot
            {
                ProcessId = tspResponse.ProcessId,
                AlorithmType = tspResponse.AlorithmType,
                Status = tspResponse.Status,
                Iteration = tspResponse.Iteration,
                Fitness = tspResponse.Fitness,
                CreatedAt = DateTime.Now,
                ElapsedTime = tspResponse.ElapsedTime,
                Route = tspResponse.Route
            };
        }
    }
}
