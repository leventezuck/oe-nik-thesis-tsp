﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TspApi.Models.GeneticAlgorithm
{
    public class Population
    {
        public float Fitness
        {
            get
            {
                return Chromosomes.OrderBy(x => x.Fitness).First().Fitness;
            }
        }
        public List<Chromosome> Chromosomes { get; set; }

        public Population()
        {
            Chromosomes = new List<Chromosome>();
        }
    }
}
