﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TspApi.Models.GeneticAlgorithm
{
    public class Chromosome
    {
        public float Fitness
        {
            get
            {
                float sumOfDistance = 0f;

                for (int i = 0; i < Cities.Count - 1; i++)
                {
                    City c1 = Cities[i];
                    City c2 = Cities[i + 1];

                    sumOfDistance += (float)Math.Sqrt(Math.Pow(c1.X - c2.X, 2) + Math.Pow(c1.Y - c2.Y, 2));
                }

                return sumOfDistance;
            }
        }

        public List<City> Cities { get; set; }

        public Chromosome()
        {
            Cities = new List<City>();
        }

        public Chromosome Clone()
        {
            return new Chromosome()
            {
                Cities = Cities.Select(x => x.Clone()).ToList()
            };
        }
    }
}
