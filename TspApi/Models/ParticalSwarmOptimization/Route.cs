﻿using System;
using System.Collections.Generic;

namespace TspApi.Models.ParticalSwarmOptimization
{
    public class Route
    {
        public List<City> Cities { get; set; }

        public float Distance
        {
            get
            {
                float sumOfDistance = 0f;

                for (int i = 0; i < Cities.Count - 1; i++)
                {
                    City c1 = Cities[i];
                    City c2 = Cities[i + 1];

                    sumOfDistance += (float)Math.Sqrt(Math.Pow(c1.X - c2.X, 2) + Math.Pow(c1.Y - c2.Y, 2));
                }

                return sumOfDistance;
            }
        }

        public Route(Route route)
        {
            Cities = new List<City>();

            foreach (City city in route.Cities)
            {
                Cities.Add(city);
            }
        }

        public Route(List<City> cities)
        {
            Cities = new List<City>();

            foreach (City city in cities)
            {
                Cities.Add(city);
            }
        }
    }
}
