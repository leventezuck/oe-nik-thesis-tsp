﻿using System.Collections.Generic;

namespace TspApi.Models.ParticalSwarmOptimization
{
    public class Location
    {
        public List<float> Locations { get; set; }

        public Location()
        {
            Locations = new List<float>();
        }

        public Location(List<float> locations)
        {
            Locations = new List<float>();

            foreach (float location in locations)
            {
                Locations.Add(location);
            }
        }
    }
}
