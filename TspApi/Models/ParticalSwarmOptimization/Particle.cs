﻿using System;

namespace TspApi.Models.ParticalSwarmOptimization
{
    public class Particle
    {
        private static Random rnd = new Random();

        public Route Route { get; set; }
        public Location Location { get; set; }
        public Velocity Velocity { get; set; }

        public Route PBest { get; set; }
        public Location LocationPBest { get; set; }

        public static int ProblemSize { get; private set; }

        public Particle(Route route)
        {
            Route = route;
            PBest = route;
            Location = new Location();

            LocationPBest = new Location();
            Velocity = new Velocity();

            ProblemSize = route.Cities.Count;
        }

        public Route GetBest()
        {
            Route neighbourRoute = null;

            for (int i = 0; i < Logic.ParticalSwarmOptimization.NEIGHBORS; i++)
            {
                neighbourRoute = GetNeighborSolution(PBest);

                if (neighbourRoute.Distance < PBest.Distance)
                {
                    PBest = neighbourRoute;
                }
            }

            return PBest;
        }

        private Route GetNeighborSolution(Route route)
        {
            return SwapTwoCities(route);
        }

        public void SwapWithLocation()
        {
            for (int i = 0; i < rnd.Next(1, PBest.Cities.Count / 10); i++)
            {
                PBest = SwapTwoCities(PBest);
            }
        }

        private Route SwapTwoCities(Route route)
        {
            Route newRoute = new Route(route);

            int random1 = rnd.Next(0, PBest.Cities.Count);
            int random2 = rnd.Next(0, PBest.Cities.Count);

            while (random1 == random2)
            {
                random1 = rnd.Next(0, PBest.Cities.Count);
                random2 = rnd.Next(0, PBest.Cities.Count);
            }

            City city1 = PBest.Cities[random1];
            City city2 = PBest.Cities[random2];

            PBest.Cities[random1] = city2;
            PBest.Cities[random2] = city1;

            return newRoute;
        }
    }
}
