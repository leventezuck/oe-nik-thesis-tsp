﻿using System.Collections.Generic;

namespace TspApi.Models.ParticalSwarmOptimization
{
    public class Velocity
    {
        public List<float> Dimensions { get; set; }

        public Velocity()
        {
            Dimensions = new List<float>();
        }

        public Velocity(List<float> dimensions)
        {
            Dimensions = dimensions;
        }
    }
}
