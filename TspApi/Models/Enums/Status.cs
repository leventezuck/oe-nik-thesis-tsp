﻿namespace TspApi.Models.Enums
{
    public enum Status
    {
        running, stopped
    }
}
