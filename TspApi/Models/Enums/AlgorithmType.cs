﻿namespace TspApi.Models.Enums
{
    public enum AlgorithmType
    {
        Genetic, ParticalSwarmOptimization, SimulatedAnnealing, NearestNeighbourBasic, NearestNeighbourFull
    }
}
