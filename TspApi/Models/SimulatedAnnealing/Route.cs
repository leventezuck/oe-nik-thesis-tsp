﻿using System;
using System.Collections.Generic;

namespace TspApi.Models.SimulatedAnnealing
{
    public class Route
    {
        public List<City> Cities { get; private set; }
        public float Distance 
        {
            get
            {
                float sumOfDistance = 0f;

                City c1, c2;

                for (int i = 0; i < Cities.Count - 1; i++)
                {
                    c1 = Cities[i];
                    c2 = Cities[i + 1];

                    sumOfDistance += (float)Math.Sqrt(Math.Pow(c1.X - c2.X, 2) + Math.Pow(c1.Y - c2.Y, 2));
                }

                return sumOfDistance;
            }
        }

        public Route(Route route)
        {
            LoadCities(route.Cities);
        }

        public Route(List<City> cities)
        {
            LoadCities(cities);
        }

        private void LoadCities(List<City> cities)
        {
            Cities = new List<City>();

            foreach (City city in cities)
            {
                Cities.Add(city);
            }
        }
    }
}
