﻿namespace TspApi.Models
{
    public class City
    {
        public City(float x, float y)
        {
            X = x;
            Y = y;
        }

        public float X { get; private set; }
        public float Y { get; private set; }

        public City Clone()
        {
            return new City(X, Y);
        }
    }
}