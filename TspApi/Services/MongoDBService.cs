﻿using MongoDB.Driver;

namespace TspApi.Services
{
    public sealed class MongoDBService
    {
        private static MongoDBService instance;

        public static MongoDBService Instance { get { return instance ??= new MongoDBService(); } }

        public IMongoDatabase Database { get; private set; }

        private MongoDBService()
        {
            MongoClient mongo = new MongoClient();

            Database = mongo.GetDatabase("TSP_API_DB");
        }
    }
}
