﻿using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using TspApi.Models.DatabaseEntities;

namespace TspApi.Services
{
    public sealed class SnapshotRepository
    {
        private static SnapshotRepository instance;

        public static SnapshotRepository Instance { get { return instance ??= new SnapshotRepository(); } }

        private IMongoCollection<Snapshot> collection;

        private SnapshotRepository()
        {
            collection = MongoDBService.Instance.Database.GetCollection<Snapshot>("snapshots");
        }

        public IEnumerable<Snapshot> Get(string processId)
        {
            return collection.Find(x => x.ProcessId == processId).ToEnumerable().OrderBy(x => x.Iteration);
        }

        public void Create(Snapshot snapshot)
        {
            collection.InsertOne(snapshot);
        }
    }
}
