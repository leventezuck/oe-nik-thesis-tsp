﻿using MongoDB.Driver;
using System.Collections.Generic;
using TspApi.Models.DatabaseEntities;

namespace TspApi.Services
{
    public sealed class ProcessRepository
    {
        private static ProcessRepository instance;

        public static ProcessRepository Instance { get { return instance ??= new ProcessRepository(); } }

        private IMongoCollection<Process> collection;

        private ProcessRepository()
        {
            collection = MongoDBService.Instance.Database.GetCollection<Process>("processes");
        }

        public IEnumerable<Process> Get()
        {
            return collection.Find(x => true).ToEnumerable();
        }

        public Process Get(string id)
        {
            return collection.Find(x => x.Id == id).FirstOrDefault();
        }

        public void Create(Process process)
        {
            collection.InsertOne(process);
        }

        public void Update(Process process)
        {
            collection.ReplaceOne(x => x.Id == process.Id, process);
        }

        public void Delete(Process process)
        {
            collection.DeleteOne(x => x.Id == process.Id);
        }

        public void Delete(string id)
        {
            collection.DeleteOne(x => x.Id == id);
        }
    }
}
